/*
 * Spi.cpp
 *
 *  Created on: 21.04.2016
 *      Author: schnake
 */

#include "Spi.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <fcntl.h>
#include <unistd.h>
#include "string.h"

#include <linux/types.h>
#include <linux/spi/spidev.h>

#include <log4cpp/Category.hh>

#include <string>
#include "Exception.h"
#include <strings.h>

using namespace std;

mutex Spi::m_lock;

Spi::Spi()
{
	m_fd = -1;

	m_mode = 0;
	m_bits = 0;
	m_speed = 0;
	m_delay = 10;
}
Spi::Spi(string dev, uint8_t mode, uint8_t bits, uint32_t speed)
{
	m_fd = -1;
	m_delay = 10;

	setDevice(dev);
	init(mode, bits, speed);
}

Spi::~Spi()
{
	if(m_fd > 0) {
		close(m_fd);
	}
}


void Spi::setDevice(string dev)
{
	m_dev = dev;
}


void Spi::init(uint8_t m, uint8_t b, uint32_t s)
{
	m_mode = m;
	m_bits = b;
	m_speed = s;

	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.lt8900v2");
	logger.debug("Open SPI device %s, mode/bits/speed (%d,%d,%d)", m_dev.c_str(), m_mode, m_bits, m_speed);

	lock_guard<mutex> lg(m_lock);
	m_fd = open(m_dev.c_str(), O_RDWR);
	if(m_fd < 0) {
		logger.fatal("Could not open SPI device %s: %s", m_dev.c_str(), strerror(m_fd));
        throw Exception("Could not open SPI device "+ m_dev);
	}

	uint8_t mode = m_mode;
	int ret = ioctl(m_fd, SPI_IOC_WR_MODE, &mode);
	if(ret == -1) {
		throw Exception("can't set spi mode");
	}
	ret = ioctl(m_fd, SPI_IOC_RD_MODE, &m_mode);
	if(ret == -1) {
		throw Exception("can't get spi mode");
	}

	uint8_t bits = m_bits;
	ret = ioctl(m_fd, SPI_IOC_WR_BITS_PER_WORD, &bits);
	if(ret == -1) {
		throw Exception("can't set bits per word");
	}
	ret = ioctl(m_fd, SPI_IOC_RD_BITS_PER_WORD, &bits);
	if(ret == -1) {
		throw Exception("can't get bits per word");
	}

	uint32_t speed = m_speed;
	ret = ioctl(m_fd, SPI_IOC_WR_MAX_SPEED_HZ, &speed);
	if(ret == -1) {
		throw Exception("can't set max speed hz");
	}
	ret = ioctl(m_fd, SPI_IOC_RD_MAX_SPEED_HZ, &speed);
	if(ret == -1) {
		throw Exception("can't get max speed hz");
	}
}

int Spi::transfer(uint8_t *tx, uint8_t *rx, unsigned int len) const
{
	struct spi_ioc_transfer tr;
	bzero((void*)&tr, sizeof(struct spi_ioc_transfer));
	tr.tx_buf = (__u64)tx;
	tr.rx_buf = (__u64)rx;
	tr.len = len;
	tr.delay_usecs = m_delay;
	tr.speed_hz = m_speed;
	tr.bits_per_word = m_bits;
	tr.cs_change = 0;

	lock_guard<mutex> lg(m_lock);
	int ret = ioctl(m_fd, SPI_IOC_MESSAGE(1), &tr);
	if(ret < 1) {
		throw Exception("Can not send SPI message");
	}

	return len;
}
