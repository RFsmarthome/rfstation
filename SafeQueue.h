/*
 * SafeQueue.h
 *
 *  Created on: 25.09.2016
 *      Author: schnake
 */

#ifndef SAFEQUEUE_H_
#define SAFEQUEUE_H_

#include <mutex>
#include <queue>
#include <condition_variable>

#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>
#include <log4cpp/BasicConfigurator.hh>

template<class T>
class SafeQueue
{
public:
	SafeQueue() {};
	virtual ~SafeQueue() {};

	void enqueue(const T& element) {
		log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.SafeQueue.enqueue");
		std::unique_lock<std::mutex> lock(m_mutex);
		logger.debug("Push element");
		m_queue.push(element);
		logger.debug("Notify all");
		m_cv.notify_all();
	}
	T dequeue() {
		log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.SafeQueue.dequeue");
		std::unique_lock<std::mutex> lock(m_mutex);
		while(m_queue.empty()) {
			logger.debug("Queue is empty");
			m_cv.wait(lock);
		}
		logger.debug("Has element");
		T element = m_queue.front();
		m_queue.pop();
		return element;
	}

private:
	std::queue<T> m_queue;
	std::mutex m_mutex;
	std::condition_variable m_cv;
};

#endif /* SAFEQUEUE_H_ */
