/*
 * Spi.h
 *
 *  Created on: 21.04.2016
 *      Author: schnake
 */

#ifndef SPI_H_
#define SPI_H_

#include <string>
#include <mutex>

class Spi {
public:
	Spi();
	Spi(std::string dev, uint8_t mode, uint8_t bits, uint32_t speed);
	virtual ~Spi();

	void init(uint8_t m, uint8_t b, uint32_t s);
	void setDevice(std::string dev);

	int transfer(uint8_t *tx, uint8_t *rx, unsigned int len) const;

protected:
	std::string m_dev;
	int m_fd;
	uint8_t m_mode;
	uint8_t m_bits;
	uint32_t m_speed;
	uint16_t m_delay;

	static std::mutex m_lock;
};

#endif /* SPI_H_ */
