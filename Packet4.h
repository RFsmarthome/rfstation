/*
 * Packet.h
 *
 *  Created on: 06.03.2016
 *      Author: schnake
 */

#ifndef PACKET4_H_
#define PACKET4_H_

#include "schnakebus.h"

#include <string>
#include <memory>

class MqttClient;

class Packet4
{
public:
	Packet4();
	Packet4(const buspacket4_t &p);
	Packet4(const char *buffer);
	virtual ~Packet4();

	void setSource(uint8_t s);
	uint8_t getSource() const;

	void setDestination(uint8_t d);
	uint8_t getDestination() const;

	void setAck(bool a);
	bool getAck() const;

	void setReqAck(bool a);
	bool getReqAck() const;

	void setType(uint8_t type);
	uint8_t getType() const;

	void setSensorId(uint8_t sensorid);
	uint8_t getSensorId() const;

	void setBuspacket(const buspacket4_t &p);
	const buspacket4_t& getBuspacket() const;

	buspacket4_t getGeneratedBuspacket(uint8_t source = 0) const;

	bool isCrcCorrect() const;
	bool isCorrect() const;

	void dump() const;
	void dump(const buspacket4_t &p) const;

	bool sendPacketToMqtt(std::shared_ptr<MqttClient> mqtt);

private:
	struct buspacket4_t m_buspacket;
	uint8_t m_destination;
};


#endif /* PACKET_H_ */
