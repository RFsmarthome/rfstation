/*
 * Exception.cpp
 *
 *  Created on: 21.04.2016
 *      Author: schnake
 */

#include "Exception.h"

Exception::Exception() : std::exception()
{

}

Exception::Exception(const std::string &reason) : std::exception()
{
	m_reason = reason;
}

Exception::~Exception()
{

}

const char* Exception::what() const throw()
{
	return m_reason.c_str();
}
