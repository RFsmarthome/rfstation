/*
 * Packet.cpp
 *
 *  Created on: 06.03.2016
 *      Author: schnake
 */

#include <ios>
#include <iostream>
#include <sstream>
#include <string>
#include <cstring>
#include <log4cpp/Category.hh>

#include "Packet.h"
#include "MqttClient.h"
#include "crc8.h"

using namespace std;

Packet::Packet()
{
	m_buspacket.source = 0;
	m_buspacket.ack = false;
	m_buspacket.req_ack = false;

	m_destination = 0;
}

Packet::Packet(const buspacket_t &p)
{
	m_buspacket = p;
	m_destination = 0;
}

Packet::Packet(const char *buffer)
{
	memcpy(&m_buspacket, buffer, sizeof(buspacket_t));
}

Packet::~Packet()
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.Packet");

	logger.debug("Destructor called");
}

void Packet::setSource(uint8_t s)
{
	m_buspacket.source = s;
}
uint8_t Packet::getSource() const
{
	return m_buspacket.source;
}

void Packet::setAck(bool a)
{
	m_buspacket.ack = a;
}
bool Packet::getAck() const
{
	return m_buspacket.ack;
}

void Packet::setReqAck(bool a)
{
	m_buspacket.req_ack = a;
}
bool Packet::getReqAck() const
{
	return m_buspacket.req_ack;
}

uint8_t Packet::getType() const
{
	return m_buspacket.type;
}

void Packet::setBuspacket(const buspacket_t &p)
{
	m_buspacket = p;
}
const buspacket_t& Packet::getBuspacket() const
{
	return m_buspacket;
}

buspacket_t Packet::getGeneratedBuspacket(uint8_t source) const
{
	buspacket_t p;

	p = m_buspacket;

	p.version = SB_VERSION;
	p.packet = 0;
	p.source = source;

	Crc8 crc;

	p.crc8 = crc.message((uint8_t*)&p, sizeof(buspacket_t)-1);

	return p;
}

bool Packet::isCrcCorrect() const
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.Packet.isCrcCorrect");

	Crc8 crc;
	uint8_t rc = crc.message((uint8_t*)&m_buspacket, sizeof(buspacket_t));

	logger.debug("Packet CRC calc: 0x%02x", (unsigned int)rc);
	logger.debug("Packet CRC in packet: 0x%02x", (unsigned int)m_buspacket.crc8);

	return(rc==0);
}

bool Packet::isCorrect() const
{
	return isCrcCorrect() && m_buspacket.version<=VERSION3;
}

void Packet::dump() const
{
	dump(this->m_buspacket);
}

void Packet::dump(const buspacket_t &p) const
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.Packet.dumpPacket");

	logger.debug("Version: %d", p.version);
	logger.debug("Type: %d", p.type);
	logger.debug("Source: %d", p.source);
	logger.debug("Packet: %d", p.packet);
	logger.debug("Ack: %d", p.ack);
	logger.debug("ReqAck: %d", p.req_ack);
	logger.debug("CRC: 0x%02x", p.crc8);

	switch(p.type) {
	case TYPE_BMP180:
		logger.debug("BMP180.pressure: %u", p.bmp180.pressure);
		logger.debug("BMP180.temperature: %d", p.bmp180.temperature);
		break;
	case TYPE_DHT22:
		logger.debug("DHT22.humanity: %u", p.dht22.humanity);
		logger.debug("DHT22.temperature: %d",p.dht22.temperature);
		break;
	case TYPE_BATTERY:
		logger.debug("BATTERY.battery: %u", p.battery.battery);
		logger.debug("BATTERY.battery_raw: %u", p.battery.battery_raw);
		break;
	case TYPE_ON_OFF:
		logger.debug("ON_OFF.onoff: %u", p.onoff.onoff);
		break;
	case TYPE_RGB:
		logger.debug("RGB.rgb: %d %d %d", p.rgb.r, p.rgb.g, p.rgb.b);
		break;
	case TYPE_DIMMER:
		logger.debug("DIMMER: %d", p.dimmer.dimmer);
		break;
	case TYPE_WRGB1:
		logger.debug("WRGB1: %u %u %u %u", p.wrgb1.w1, p.wrgb1.r1, p.wrgb1.g1, p.wrgb1.b1);
		break;
	case TYPE_WRGB2:
		logger.debug("WRGB2: %u %u %u %u / %u %u %u %u", p.wrgb2.w1, p.wrgb2.r1, p.wrgb2.g1, p.wrgb2.b1, p.wrgb2.w2, p.wrgb2.r2, p.wrgb2.g2, p.wrgb2.b2);
		break;
	case TYPE_TEMPERATURE:
		logger.debug("TEMPERATURE: %d", p.temperature.temperature);
		break;
	case TYPE_PRESSURE:
		logger.debug("PRESSURE: %d", p.pressure.pressure);
		break;
	case TYPE_HUMIDITY:
		logger.debug("HUMIDITY: %d", p.humidity.humidity);
		break;
	}
}

void Packet::setDestination(uint8_t d)
{
	m_destination = d;
}

uint8_t Packet::getDestination() const
{
	return m_destination;
}

bool Packet::sendPacketToMqtt(shared_ptr<MqttClient> mqtt)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.Packet.sendPacketToMqtt");

	if(!isCorrect()) {
		logger.info("Packet not correct, not sending");
		return false;
	}

	int mid;
	switch(getType()) {
	case TYPE_BMP180:
		{
			logger.info("BMP180");
			string topic1 = to_string(getSource()) + "/BMP180/pressure";
			string payload1 = to_string(getBuspacket().bmp180.pressure/100.0);
			mqtt->publish(mid, topic1, payload1);

			string topic2 = to_string(getSource()) + "/BMP180/temperature";
			string payload2 = to_string(getBuspacket().bmp180.temperature/10.0);
			mqtt->publish(mid, topic2, payload2);
		}

		break;
	case TYPE_DHT22:
		{
			logger.info("DHT22");
			string topic1 = to_string(getSource()) + "/DHT22/humanity";
			string payload1 = to_string(getBuspacket().dht22.humanity/10.0);
			mqtt->publish(mid, topic1, payload1);

			string topic2 = to_string(getSource()) + "/DHT22/temperature";
			string payload2 = to_string(getBuspacket().dht22.temperature/10.0);
			mqtt->publish(mid, topic2, payload2);
		}

		break;
	case TYPE_BATTERY:
		{
			logger.info("BATTERY");
			string topic1 = to_string(getSource()) + "/BATTERY/battery";
			string payload1 = to_string(getBuspacket().battery.battery/1000.0); // in mV
			mqtt->publish(mid, topic1, payload1);

			string topic2 = to_string(getSource()) + "/BATTERY/battery_raw";
			string payload2 = to_string(getBuspacket().battery.battery_raw); // RAW value from ADC
			mqtt->publish(mid, topic2, payload2);
		}

		break;
	case TYPE_ON_OFF:
		{
			logger.info("ON_OFF");
			string topic1 = to_string(getSource()) + "/ON_OFF/onoff";
			string payload1;
			if(getBuspacket().onoff.onoff==0) {
				payload1 = "OFF";
			} else {
				payload1 = "ON";
			}
			mqtt->publish(mid, topic1, payload1);
			break;
		}
	case TYPE_TEMPERATURE:
		{
			logger.info("TEMPERATURE");
			string topic1 = to_string(getSource()) + "/TEMPERATURE/temperature";
			string payload1 = to_string(getBuspacket().temperature.temperature/10.0);
			mqtt->publish(mid, topic1, payload1);
			break;
		}
	case TYPE_PRESSURE:
		{
			logger.info("PRESSURE");
			string topic1 = to_string(getSource()) + "/PRESSURE/pressure";
			string payload1 = to_string(getBuspacket().pressure.pressure/100.0);
			mqtt->publish(mid, topic1, payload1);
			break;
		}
	case TYPE_HUMIDITY:
		{
			logger.info("HUMIDITY");
			string topic1 = to_string(getSource()) + "/HUMIDITY/humidity";
			string payload1 = to_string(getBuspacket().humidity.humidity/10.0);
			mqtt->publish(mid, topic1, payload1);
			break;
		}
	}

	return true;
}
