/*
 * MqttClient.cpp
 *
 *  Created on: 05.03.2016
 *      Author: schnake
 */

#include <string>
#include <memory>
#include <regex>
#include <mosquittopp.h>
#include <vector>

#include <log4cpp/Category.hh>

#include "Packet.h"
#include "MqttClient.h"
#include "SafeQueue.h"
//#include "Lt8900v2.h"

/*
#include "RfSend.h"
#include "Packet.h"
*/

using namespace std;
using namespace mosqpp;

MqttClient::MqttClient(shared_ptr<SafeQueue<Packet*>> queue, const Configuration &config) : mosquittopp("rfstation", true)
{
	lock_guard<mutex> lock(m_mutex);
	m_baseTopic = config.getMqttBase();

	m_keepalive = 60;
	m_isConnected = false;

	m_qos_pub = config.getMqttQos();
	m_qos_sub = config.getMqttQos();
	m_retain = config.isMqttRetain();

	m_disconnect = false;

	m_queue = queue;

	lib_init();
	//loop_start();
	connect(config.getMqttHost().c_str(), config.getMqttPort(), m_keepalive);
}

MqttClient::~MqttClient()
{
	lock_guard<mutex> lock(m_mutex);
	destroy();
}

void MqttClient::destroy()
{
	m_disconnect = true;
	disconnect();
	lib_cleanup();
}

void MqttClient::on_connect(int rc)
{
	lock_guard<mutex> lock(m_mutex);
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.MqttClient.on_connect");
	logger.debug("rc: (%d)", rc);

	if(rc==0) {
		m_isConnected = true;
	} else {
		m_isConnected = false;
	}
}

void MqttClient::on_disconnect(int rc)
{
}

bool MqttClient::isConnected() const
{
	return m_isConnected;
}

int MqttClient::publish(int &mid, const std::string &topic, const std::string &payloadString)
{
	lock_guard<mutex> lock(m_mutex);
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.MqttClient.publish");
	logger.debug("Publish: (%s): %s", topic.c_str(), payloadString.c_str());

	return mosquittopp::publish(&mid, (m_baseTopic+"/"+topic).c_str(), (int)payloadString.size(), payloadString.c_str(), m_qos_pub, m_retain);
}

int MqttClient::subscribe(int &mid, const std::string &topic)
{
	lock_guard<mutex> lock(m_mutex);
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.MqttClient.subscribe");
	logger.debug("Subscribe: (%s)", (m_baseTopic+"/"+topic).c_str());
	return mosquittopp::subscribe(&mid, (m_baseTopic+"/"+topic).c_str(), m_qos_sub);
}

bool MqttClient::packetRGB(string payloadString, Packet* p)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.MqttClient.packetRGB");

	buspacket_t bp;
	bp.type = TYPE_RGB;
	regex re("[;]+");
	const sregex_token_iterator end;
	sregex_token_iterator it(payloadString.begin(), payloadString.end(), re, -1);
	if(it==end) return false;
	bp.rgb.r = stoi(it->str());
	if(++it==end) return false;
	bp.rgb.g = stoi(it->str());
	if(++it==end) return false;
	bp.rgb.b = stoi(it->str());
	p->setBuspacket(bp);
	logger.debug("RGB: %d %d %d", bp.rgb.r, bp.rgb.g, bp.rgb.b);
	return true;
}

bool MqttClient::packetDIMMER(const string& payloadString, Packet* p)
{
	buspacket_t bp;
	bp.type = TYPE_DIMMER;
	bp.dimmer.dimmer = stoi(payloadString);
	p->setBuspacket(bp);
	return true;
}

bool MqttClient::packetWRGB1(string payloadString, Packet* p)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.MqttClient.packetWRGB1");

	buspacket_t bp;
	bp.type = TYPE_WRGB1;
	regex re("[;]+");
	const sregex_token_iterator end;
	sregex_token_iterator it(payloadString.begin(), payloadString.end(), re, -1);
	if(it==end) return false;
	bp.wrgb1.channel = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb1.w1 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb1.r1 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb1.g1 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb1.b1 = stoi(it->str());
	p->setBuspacket(bp);
	logger.debug("WRGB1: %u %u %u %u", bp.wrgb1.w1, bp.wrgb1.r1, bp.wrgb1.g1, bp.wrgb1.b1);
	return true;
}

bool MqttClient::packetWRGB2(string payloadString, Packet* p)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.MqttClient.packetWRGB2");

	buspacket_t bp;
	bp.type = TYPE_WRGB2;
	regex re("[;]+");
	const sregex_token_iterator end;
	sregex_token_iterator it(payloadString.begin(), payloadString.end(), re, -1);
	if(it==end) return false;
	bp.wrgb2.w1 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb2.r1 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb2.g1 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb2.b1 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb2.w2 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb2.r2 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb2.g2 = stoi(it->str());
	if(++it==end) return false;
	bp.wrgb2.b2 = stoi(it->str());
	p->setBuspacket(bp);
	logger.debug("WRGB2: %u %u %u %u / %u %u %u %u", bp.wrgb2.w1, bp.wrgb2.r1, bp.wrgb2.g1, bp.wrgb2.b1, bp.wrgb2.w2, bp.wrgb2.r2, bp.wrgb2.g2, bp.wrgb2.b2);
	return true;
}

void MqttClient::on_message(const struct mosquitto_message *message)
{
	lock_guard<mutex> lock(m_mutex);
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.MqttClient.on_message");

	logger.debug("MQTT Packet received");

	bool hasPacket = false;

	try {
		regex re("[/]+");
		string topic;
		topic.assign(message->topic);

		vector<string> v;

		sregex_token_iterator it(topic.begin(), topic.end(), re, -1);
		sregex_token_iterator reg_end;
		for(;it!=reg_end;++it) {
			v.push_back(it->str());
		}

		int address = stoi(v.at(v.size()-2));
		string type = v.at(v.size()-1);

		logger.debug("ADDRESS: %d", address);
		logger.debug("TYPE: %s", type.c_str());

		string payloadString;
		payloadString.assign((const char*)message->payload, message->payloadlen);

		logger.debug("topic: %s", message->topic);
		logger.debug("payloadString: %s", payloadString.c_str());

		Packet *p = new Packet();
		p->setSource(0);
		p->setDestination(address);

		if(type.compare("RGB")==0) {
			hasPacket = packetRGB(payloadString, p);
		} else if(type.compare("DIMMER")==0) {
			hasPacket = packetDIMMER(payloadString, p);
		} else if(type.compare("WRGB1")==0) {
			hasPacket = packetWRGB1(payloadString, p);
		} else if(type.compare("WRGB2")==0) {
			hasPacket = packetWRGB2(payloadString, p);
		}

		if(hasPacket) {
			m_queue->enqueue(p);
		}
	}
	catch(std::exception &ex) {
		logger.error("Unexcepted MQTT message received. Exception what: (%s)", ex.what());
	}
}
