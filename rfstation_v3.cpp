//============================================================================
// Name        : rfstation_v2.cpp
// Author      : Marcus Schneider <schnake24@gmail.com>
// Version     :
// Copyright   : Copyrigh(c)2016 by Marcus Schneider GPLv3
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <chrono>
#include <thread>
#include <libconfig.h++>
//#include <pigpiod_if2.h>
#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>
#include <log4cpp/BasicConfigurator.hh>
#include <unistd.h>
#include <mutex>

using namespace std;

#include "Configuration.h"

#include "Lt8900v2.h"
#include "Packet.h"
#include "Packet4.h"
#include "MqttClient.h"
#include "SafeQueue.h"
#include "schnakebus.h"

#define RECEIVE_BUFFER_SIZE 70

void receiveLoop(shared_ptr<MqttClient> &mqtt, shared_ptr<SafeQueue<Packet*>> &queue, shared_ptr<Lt8900v2> &lt8900)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.rfstation.receiveLoop");

	while(1) {
		{
			lock_guard<mutex> lock(lt8900->getMutex());
			lt8900->toListen();
		}
		if(lt8900->waitForPacket(120*1000)==1) {
			logger.debug("Edge detected!");
			char *buffer = new char[RECEIVE_BUFFER_SIZE];
			unsigned int packetLength;
			{
				lock_guard<mutex> lock(lt8900->getMutex());
				packetLength = lt8900->receive(buffer, RECEIVE_BUFFER_SIZE);

				logger.debug("Packet length: %d", packetLength);
				if(packetLength==sizeof(buspacket_t)) {
					logger.debug("Could be a valid packet");
					Packet packet(buffer);
					packet.dump();
					if(packet.isCorrect()) {
						packet.sendPacketToMqtt(mqtt);
					}
				} else if(packetLength==sizeof(buspacket4_t)) {
					logger.debug("Could be a valid packet4");
					Packet4 packet4(buffer);
					packet4.dump();
					if(packet4.isCorrect()) {
						packet4.sendPacketToMqtt(mqtt);
					}
				} else {
					logger.debug("Timeout for receive loop");
				}
			}
			delete[] buffer;
		}
	}
}

void sendLoop(shared_ptr<MqttClient> &mqtt, shared_ptr<SafeQueue<Packet*>> &queue, shared_ptr<Lt8900v2> &lt8900)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.rfstation.sendLoop");

	while(1) {
		Packet *p = queue->dequeue();
		{
			lock_guard<mutex> lock(lt8900->getMutex());
			buspacket_t packet = p->getGeneratedBuspacket();
			logger.debug("Dump sending packet");
			p->dump(packet);
			lt8900->send(p->getDestination(), &packet, sizeof(buspacket_t));

			usleep(10000);

			lt8900->clearFifo();
			lt8900->toListen();
			delete p;
		}
	}
}

void mqttSubscribe(shared_ptr<MqttClient> mqtt)
{
	int mid;
	mqtt->subscribe(mid, "send/#");
}

int main(int argv, char** argc)
{
	try {
		Configuration config("rfstation.properties");

		log4cpp::PropertyConfigurator::configure(config.getLog4cppFilename());
		log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.rfstation.main");

		shared_ptr<SafeQueue<Packet*>> queue(new SafeQueue<Packet*>());

		shared_ptr<MqttClient> mqtt(new MqttClient(queue, config));

		logger.debug("Init lt8900 sender begin");
		shared_ptr<Lt8900v2> lt8900(new Lt8900v2(config));
		lt8900->reset();
		lt8900->whatsUp();
		if(lt8900->testHardware()) {
			logger.debug("OKAY: Everything looks fine");
		} else {
			lt8900->whatsUp();
			logger.warn("ERROR: Not so fine");
		}
		lt8900->initHardware();
		lt8900->setPower(15, 4);

		logger.debug("Subscribe to MQTT");
		mqttSubscribe(mqtt);

		logger.debug("Start threads");
		thread sendThread(sendLoop, ref(mqtt), ref(queue), ref(lt8900));
		thread receiveThread(receiveLoop, ref(mqtt), ref(queue), ref(lt8900));

		logger.debug("Start main loop");
		int rc;
		while(1) {
			rc = mqtt->loop();
			if(rc) {
				logger.info("Reconnect MQTT client");

				mqtt->reconnect();
				mqttSubscribe(mqtt);
			}
		}

		logger.debug("End");
	}
	catch(libconfig::ParseException &e) {
		cerr << "Error:" << e.getError() << endl << "Line:" << e.getLine() << endl;;
	}
	return 0;
}
