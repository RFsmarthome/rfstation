/*
 * MqttClient.h
 *
 *  Created on: 05.03.2016
 *      Author: schnake
 */

#ifndef MQTTCLIENT_H_
#define MQTTCLIENT_H_

#include <mosquittopp.h>
#include <string>
#include "SafeQueue.h"
#include "Packet.h"
#include "Configuration.h"

class MqttClient : public mosqpp::mosquittopp
{
public:
	MqttClient(std::shared_ptr<SafeQueue<Packet*>> queue, const Configuration &config);
	virtual ~MqttClient();

	void destroy();

	bool isConnected() const;
	int publish(int &mid, const std::string &topic, const std::string &payloadString);
	int subscribe(int &mid, const std::string &topic);

	void on_connect(int rc);
	void on_message(const struct mosquitto_message *message);
	void on_disconnect(int rc);

private:

	unsigned int m_keepalive;
	bool m_isConnected;
	std::string m_baseTopic;

	int m_qos_pub;
	int m_qos_sub;
	bool m_retain;

	bool m_disconnect;

	std::shared_ptr<SafeQueue<Packet*>> m_queue;
	std::mutex m_mutex;

	bool packetRGB(std::string payloadString, Packet* p);
	bool packetDIMMER(const std::string& payloadString, Packet* p);
	bool packetWRGB1(std::string payloadString, Packet* p);
	bool packetWRGB2(std::string payloadString, Packet* p);
};

#endif /* MQTTCLIENT_H_ */
