/*
 * Configuration.cpp
 *
 *  Created on: 26.09.2016
 *      Author: schnake
 */

#include "Configuration.h"

#include <log4cpp/Category.hh>
#include <log4cpp/PropertyConfigurator.hh>
#include <log4cpp/BasicConfigurator.hh>

#include <string>
#include <libconfig.h++>

using namespace std;


Configuration::Configuration(const string &filename) : m_filename(filename)
{
	libconfig::Config config;
	config.readFile(filename.c_str());

	m_log4cppFilename = config.lookup("log4cpp.file").c_str();

	m_mqttHost = config.lookup("mqtt.host").c_str();
	m_mqttBase = config.lookup("mqtt.base").c_str();
	m_mqttPort = config.lookup("mqtt.port");
	m_mqttQos = config.lookup("mqtt.qos");
	m_mqttRetain = config.lookup("mqtt.retain");

	m_lt8900SpiDev = config.lookup("lt8900.spi.device").c_str();
	m_lt8900Channel = config.lookup("lt8900.channel");
	m_lt8900SpiFrequency = config.lookup("lt8900.spi.frequency");
	m_lt8900PinReset = config.lookup("lt8900.pin.reset");
	m_lt8900PinPacket = config.lookup("lt8900.pin.packet");
	m_lt89000Retransmission = config.lookup("lt8900.retransmission");

}

Configuration::~Configuration()
{
}
