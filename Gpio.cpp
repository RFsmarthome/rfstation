/*
 * Gpio.cpp
 *
 *  Created on: 23.04.2016
 *      Author: schnake
 */

#include "Gpio.h"

#include <log4cpp/Category.hh>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <poll.h>
#include <unistd.h>
#include <string.h>

#define MAX_BUF 256

using namespace std;

Gpio::Gpio()
{
	m_sysPath = "/sys/class/gpio";
}

Gpio::~Gpio()
{
}

/****************************************************************
 * gpio_export
 ****************************************************************/
int Gpio::exportGpio(unsigned int gpio)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.gpio.exportGpio");

	int fd = ::open((m_sysPath + "/export").c_str(), O_WRONLY);
	if(fd < 0) {
		logger.fatal("Could not export GPIO pin %d", gpio);
		return fd;
	}

	char buf[MAX_BUF];
	int len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	::close(fd);

	logger.debug("GPIO pin %d exported", gpio);

	return 0;
}

/****************************************************************
 * gpio_unexport
 ****************************************************************/
int Gpio::unexport(unsigned int gpio)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.gpio.unexport");

	int fd = ::open((m_sysPath + "/unexport").c_str(), O_WRONLY);
	if(fd < 0) {
		logger.error("Could not unexport GPIO pin %d", gpio);
		perror("gpio/export");
		return fd;
	}

	char buf[MAX_BUF];
	int len = snprintf(buf, sizeof(buf), "%d", gpio);
	write(fd, buf, len);
	::close(fd);

	logger.debug("GPIO pin %d unexported", gpio);

	return 0;
}

/****************************************************************
 * gpio_set_dir
 ****************************************************************/
int Gpio::setDirection(unsigned int gpio, gpio_direction out_flag)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.gpio.setDirection");

	char buf[MAX_BUF];
	snprintf(buf, sizeof(buf), (m_sysPath + "/gpio%d/direction").c_str(), gpio);

	int fd = ::open(buf, O_WRONLY);
	if(fd < 0) {
		logger.error("Could not set output direction for GPIO %d", gpio);
		return fd;
	}

	switch(out_flag) {
	case IN:
		write(fd, "in", 3);
		break;
	case OUT:
		write(fd, "out", 4);
		break;
	}

	::close(fd);
	return 0;
}

/****************************************************************
 * gpio_set_value
 ****************************************************************/
int Gpio::setValue(unsigned int gpio, bool value)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.gpio.setValue");

	char buf[MAX_BUF];
	snprintf(buf, sizeof(buf), (m_sysPath + "/gpio%d/value").c_str(), gpio);

	int fd = ::open(buf, O_WRONLY);
	logger.debug("Set value to %d at %d", value, fd);
	if(value) {
		write(fd, "1", 2);
	} else {
		write(fd, "0", 2);
	}

	::close(fd);

	return 0;
}

/****************************************************************
 * gpio_get_value
 ****************************************************************/
bool Gpio::getValue(unsigned int gpio)
{
	char buf[MAX_BUF];
	snprintf(buf, sizeof(buf), (m_sysPath + "/gpio%d/value").c_str(), gpio);

	int fd = ::open(buf, O_RDONLY);

	char ch;
	read(fd, &ch, 1);

	if(ch != '0') {
		::close(fd);
		return true;
	}

	::close(fd);
	return false;
}

/****************************************************************
 * gpio_set_edge
 ****************************************************************/

int Gpio::setEdge(unsigned int gpio, gpio_edge edge)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.gpio.setDirection");

	char buf[MAX_BUF];
	snprintf(buf, sizeof(buf), (m_sysPath + "/gpio%d/edge").c_str(), gpio);

	int fd = ::open(buf, O_WRONLY);
	if(fd < 0) {
		logger.error("Could not set edge for GPIO %d", gpio);
		return fd;
	}

	string dir;
	switch(edge) {
	case FALLING:
		dir = "falling";
		break;
	case RISING:
		dir = "rising";
		break;
	case BOTH:
		dir = "both";
		break;
	case NONE:
		dir = "none";
		break;
	}
	write(fd, dir.c_str(), dir.length() + 1);
	::close(fd);
	return 0;
}

/****************************************************************
 * gpio_poll
 ****************************************************************/

bool Gpio::poll(unsigned int gpio, int timeout)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.gpio.poll");

	char buf[MAX_BUF];
	snprintf(buf, sizeof(buf), (m_sysPath + "/gpio%d/value").c_str(), gpio);

	logger.debug("Open GPIO %d as path %s", gpio, buf);
	int fd = ::open(buf, O_RDONLY | O_NONBLOCK);
	if(fd < 0) {
		logger.error("Could not open GPIO device %d", gpio);
		return false;
	}

	struct pollfd fdset[1];
	memset((void*) fdset, 0, sizeof(fdset));

	fdset[0].fd = fd;
	fdset[0].events = POLLPRI | POLLERR;

	logger.debug("Dummy read before %d", gpio);
	read(fdset[0].fd, buf, MAX_BUF);

	logger.debug("Poll on GPIO %d", gpio);
	int nfds = 1;
	int rc = ::poll(fdset, nfds, timeout);
	logger.debug("Poll rc(0x%x) revents(0x%x)", rc, fdset[0].revents);
	if(fdset[0].revents & POLLPRI) {
		logger.debug("Dummy after before %d", gpio);
	}

	lseek(fd, 0, SEEK_SET);
	::read(fd, buf, MAX_BUF);
	logger.debug("Buffer: 0x%x", buf[0]);

	::close(fd);

	if(buf[0]=='1') {
		return true;
	}

	return false;
}
