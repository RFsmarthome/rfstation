/*
 * Lt8900v2.h
 *
 *  Created on: 11.03.2016
 *      Author: schnake
 */

#ifndef LT8900V2_H_
#define LT8900V2_H_

#include <string>
#include <memory>
#include <mutex>
#include <condition_variable>
#include "Configuration.h"

#include "Gpio.h"
#include "Spi.h"

#define SYNCWORD 0x0123456789abcdef

class Lt8900v2
{
public:
	Lt8900v2(const Configuration &config);
	virtual ~Lt8900v2();

	void initHardware();

	uint16_t writeRegister(uint8_t reg, uint8_t data1, uint8_t data2) const;
	uint16_t writeRegister(uint8_t reg, uint16_t data) const;
	uint16_t readRegister(uint8_t reg) const;

	void setPower(unsigned int power, unsigned int gain) const;

	bool isPacket();

	bool isStatusPkt() const;
	bool isStatusCrcError() const;
	bool isStatusFec23Error() const;
	bool isStatusFifo() const;

	bool waitForPacket(int milliseconds=0);
	void sleep() const;
	void powerUp() const;

	void toListen(uint8_t address=0);
	unsigned int receive(void *buffer, unsigned int maxBufferSize);
	bool send(uint8_t address, void *buffer, unsigned int length, bool wait=true);

	bool setSyncWord(uint64_t syncword) const;
	void clearFifo()const ;

	void reset();
	bool testHardware();
	void whatsUp();

	unsigned int readFifoWritePointer();
	unsigned int readFifoReadPointer();

	uint16_t readStatusReg() const;

	std::mutex& getMutex() {
		return m_hardwareMutex;
	}

private:
	uint64_t getSyncAddress(uint8_t address) const;

	int m_spiFrequency;
	int m_pinReset;
	int m_pinPacket;
	int m_channel;
	int m_retransmission;

	Gpio m_gpio;
	Spi m_spi;

	std::mutex m_hardwareMutex;

	static std::shared_ptr<Lt8900v2> m_instance;

	volatile bool m_isActive;
};

#endif /* LT8900V2_H_ */
