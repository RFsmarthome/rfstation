# RFstation
The Raspberry PI communication base. The complet network comunicate with a LT8900 rf chip.
These chips are cheap and you can get them without any problems from china.
The LT8900 use the SPI protocol.

At the moment the protocol stack of the sensor net could handle one packet with payload which
is enough for me cause every sensor has his own packet delivery. The protocol is protected with CRC
and in the near future an encryption with the XTEA encoder will available.

The comunication to Openhab is done with the MQTT protocol.
