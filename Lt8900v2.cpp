/*
 * Lt8900v2.cpp
 *
 *  Created on: 11.03.2016
 *      Author: schnake
 */

#include "Lt8900v2.h"
#include "Packet.h"

#include <chrono>
#include <cstring>
#include <thread>
#include <log4cpp/Category.hh>
#include <mutex>
#include <condition_variable>
#include "Exception.h"
#include "Gpio.h"

using namespace std;

shared_ptr<Lt8900v2> Lt8900v2::m_instance = 0;

Lt8900v2::Lt8900v2(const Configuration &config)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.lt8900v2");

	m_spiFrequency = config.getLt8900SpiFrequency();
	m_pinReset = config.getLt8900PinReset();
	m_pinPacket = config.getLt8900PinPacket();

	m_channel = config.getLt8900Channel();
	m_retransmission = config.getLt89000Retransmission();


	m_spi.setDevice(config.getLt8900SpiDev());
	m_spi.init(1, 8, m_spiFrequency);

	logger.debug("Initialize pins: %d, %d", m_pinReset, m_pinPacket);
	// Reset to output and default to high
	m_gpio.exportGpio(m_pinReset);
	m_gpio.setDirection(m_pinReset, Gpio::OUT);
	m_gpio.setValue(m_pinReset, true);

	// Packet to input and pull down enable
	m_gpio.exportGpio(m_pinPacket);
	m_gpio.setDirection(m_pinPacket, Gpio::IN);
	m_gpio.setEdge(m_pinPacket, Gpio::RISING);
	//m_gpio.open(m_pinPacket);

	//logger.debug("Register callback");
	//callback(m_pi, m_pinPacket, RISING_EDGE, callbackFunc);
	/* TODO missing callback */
}

Lt8900v2::~Lt8900v2()
{
	//m_gpio.close(m_pinPacket);
}

bool Lt8900v2::testHardware()
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.lt8900v2.testHardware");

	logger.info("Hardware test called");
	uint8_t send[3];
	uint8_t receive[3];

	send[0] = 0x80+0x00;
	send[1] = 0x00;
	send[2] = 0x00;

	int rc = m_spi.transfer(send, receive, 3);
	logger.info("Test receive: (%d) %02x %02x %02x", 0, receive[0], receive[1], receive[2]);
	if(rc==3 && receive[0]==0x01 && receive[1]==0x6f && receive[2]==0xef) {
		readStatusReg();

		return true;
	}
	return false;
}


void Lt8900v2::reset()
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.lt8900v2.reset");

	logger.debug("reset called");

	m_gpio.setValue(m_pinReset, 0);
	this_thread::sleep_for(chrono::milliseconds(100));
	m_gpio.setValue(m_pinReset, 1);
	this_thread::sleep_for(chrono::milliseconds(100));
}

uint16_t Lt8900v2::writeRegister(uint8_t reg, uint8_t data1, uint8_t data2) const
{
	uint8_t send[3], receive[3];

	send[0] = reg & 0x7f;
	send[1] = data1;
	send[2] = data2;

	int length = m_spi.transfer(send, receive, 3);
	if(length!=3) {
		return 0xffff;
	}

	return (uint16_t)receive[1]<<8 | (uint16_t)receive[2];
}

uint16_t Lt8900v2::writeRegister(uint8_t reg, uint16_t data) const
{
	return writeRegister(reg, (uint8_t)(data>>8), (uint8_t)data&0xff);
}

uint16_t Lt8900v2::readRegister(uint8_t reg) const
{
	uint8_t send[3], receive[3];

	send[0] = reg | 0x80;
	send[1] = 0;
	send[2] = 0;

	int length = m_spi.transfer(send, receive, 3);
	if(length!=3) {
		return 0xffff;
	}

	return (uint16_t)receive[1]<<8 | (uint16_t)receive[2];
}

uint16_t Lt8900v2::readStatusReg() const
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.lt8900v2.readStatusReg");

	uint16_t flag = readRegister(48);

	logger.debug("Status: 0x%04x", flag);

	return flag;
}

void Lt8900v2::initHardware()
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.lt8900v2.initHardware");

	logger.debug("Initializing hardware");

	writeRegister(0, 0x6fe0);
	//writeRegister(0, 0x6fef);
	writeRegister(1, 0x5681);
	writeRegister(2, 0x6617);
	writeRegister(4, 0x9cc9);
	writeRegister(5, 0x6637);
	writeRegister(8, 0x6c90);    //power (default 71af) UNDOCUMENTED

	writeRegister(10, 0x7ffd);   //bit 0: XTAL OSC enable
	writeRegister(11, 0x0100);   //bit 8: Power down RSSI (1=  RSSI power down)
	writeRegister(12, 0x0000);
	writeRegister(13, 0x48bd);

	writeRegister(22, 0x00ff);
	writeRegister(23, 0x8005);
	writeRegister(24, 0x0067);
	writeRegister(25, 0x1659);
	writeRegister(26, 0x19e0);
	writeRegister(27, 0x1300);  //bits 5:0, Crystal Frequency adjust
	writeRegister(28, 0x1800);

	writeRegister(32, 0b1001100010010000);  //AAABBCCCDDEEFFFG  A preamble length, B, syncword length, c trailer length, d packet type
	//writeRegister(32, 0x5000);  //AAABBCCCDDEEFFFG  A preamble length, B, syncword length, c trailer length, d packet type
	//                  E FEC_type, F BRCLK_SEL, G reserved
	//0x5000 = 0101 0000 0000 0000 = preamble 010 (3 bytes), B 10 (48 bits)
	writeRegister(33, 0x3fc7);
	writeRegister(34, 0x2000);
	logger.debug("Retransmissions: %d", m_retransmission);
	writeRegister(35, m_retransmission<<8);  //POWER mode,  bit 8/9 on = retransmit = 3x (default)

	writeRegister(40, 0x4401);  //max allowed error bits = 0 (01 = 0 error bits)
	writeRegister(41, (1<<15) | (1<<14) | (1<<13) | (1<<12) | (1<<11)); // CRC, Scramble, Packetlen, FW_Term, Auto_ack

	writeRegister(42, 0xfdb0);
	writeRegister(43, 0x000f);

	writeRegister(9, ((4 & 0x0f) << 12) | ((0 & 0x0f) << 7)); // Set power control

	writeRegister(50, 0x0000);  //TXRX_FIFO_REG (FIFO queue)

	writeRegister(52, 0x8080); //Fifo Rx/Tx queue reset

	this_thread::sleep_for(chrono::microseconds(200));
	writeRegister(7, (1<<8));  //set TX mode.  (TX = bit 8, RX = bit 7, so RX would be 0x0080)
	this_thread::sleep_for(chrono::microseconds(2));
	logger.debug("Channel: %d", m_channel);
	writeRegister(7, m_channel & 0x7f);  // Frequency = 2402 + channel
}

bool Lt8900v2::setSyncWord(uint64_t syncword) const
{
	writeRegister(36, syncword & 0xffff);
	writeRegister(37, (syncword>>16) & 0xffff);
	writeRegister(38, (syncword>>32) & 0xffff);
	writeRegister(39, (syncword>>48) & 0xffff);
	this_thread::sleep_for(chrono::microseconds(5));
	return true;
}

void Lt8900v2::setPower(unsigned int power, unsigned int gain) const
{
	writeRegister(9, ((power&0x0f)<<12) | ((gain&0x0f)<<7));
}

unsigned int Lt8900v2::receive(void *buffer, unsigned int maxBufferSize)
{
	uint16_t status = readRegister(48);
	if((status & 0x8000)==0) {
		uint16_t data = readRegister(50);
		unsigned int length = data>>8;
		if(length>maxBufferSize) {
			return -2;
		}

		unsigned int pos = 0;
		((uint8_t*)buffer)[pos++] = data & 0xff;
		while(pos<length) {
			data = readRegister(50);
			((uint8_t*)buffer)[pos++] = data >> 8;
			if(pos<length) { // Avoid overflow
				((uint8_t*)buffer)[pos++] = data & 0xff;
			}
		}
		return length;
	}
	return -1;
}

bool Lt8900v2::send(uint8_t address, void *buffer, unsigned int length, bool wait)
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.lt8900v2.send");

	logger.debug("Prepare sending");
	setSyncWord(getSyncAddress(address));

	writeRegister(7, 0);
	writeRegister(52, 0x8000);

	logger.debug("To FIFO");
	uint8_t pos = 0;
	uint8_t msb, lsb;
	writeRegister(50, (uint8_t)length, ((uint8_t*)buffer)[pos++]);
	while(pos<length) {
		msb = ((uint8_t*)buffer)[pos++];
		lsb = ((uint8_t*)buffer)[pos++];
		writeRegister(50, msb, lsb);
		logger.debug("Write to FIFO");
	}

	logger.debug("Instruct TX");
	writeRegister(7, (m_channel&0x7f) | (1<<8));

	if(wait) {
		logger.debug("Begin wait for end of transmission");
		logger.debug("Wait for callback");
		int i=0;
		while(1) {
			if(m_gpio.getValue(m_pinPacket)==0) {
				break;
			}
			this_thread::sleep_for(chrono::microseconds(100));
			if(++i==50) {
				break;
			}
		}
		logger.debug("Transmission ready");
	}

	return true;
}

void Lt8900v2::sleep() const
{
	writeRegister(35, 1<<14);
}

void Lt8900v2::powerUp() const
{
	writeRegister(35, 0);
	this_thread::sleep_for(chrono::microseconds(10));
}

void Lt8900v2::toListen(uint8_t address)
{
	setSyncWord(getSyncAddress(address));
	writeRegister(7, m_channel & 0x7f); // Set channel and clear rx and tx;
	this_thread::sleep_for(chrono::microseconds(3));
	writeRegister(52, 0x0080); // Clear RX FIFO bit
	writeRegister(7, (m_channel & 0x7f) | (1<<7)); // Enable rx;
	this_thread::sleep_for(chrono::microseconds(5));
}

bool Lt8900v2::isPacket()
{
	return m_gpio.getValue(m_pinPacket);
}

bool Lt8900v2::waitForPacket(int milliseconds)
{
	return m_gpio.poll(m_pinPacket, milliseconds);
}

bool Lt8900v2::isStatusPkt() const
{
	return (readStatusReg() & (1<<6))!=0;
}

bool Lt8900v2::isStatusCrcError() const
{
	return (readStatusReg() & (1<<15))!=0;
}

bool Lt8900v2::isStatusFec23Error() const
{
	return (readStatusReg() & (1<<14))!=0;
}

bool Lt8900v2::isStatusFifo() const
{
	return (readStatusReg() & (1<<5))!=0;
}

unsigned int Lt8900v2::readFifoWritePointer()
{
	uint8_t rc = readRegister(52);
	return (rc>>8)&0b111111;
}

unsigned int Lt8900v2::readFifoReadPointer()
{
	uint8_t rc = readRegister(52);
	return rc&0b111111;
}

void Lt8900v2::whatsUp()
{
	log4cpp::Category &logger = log4cpp::Category::getInstance("org.schnake.lt8900v2");

	uint16_t mode = readRegister(7);
	logger.info("TX_EN=%d", (mode & (1<<8))!=0);
	logger.info("RX_EN=%d", (mode & (1<<7))!=0);
	logger.info("Channel=%d", (mode & 0b01111111));

	uint16_t state = readRegister(48);
	bool crc_error = state & (1<<15);
	bool fec23_error = state & (1<<14);
	uint8_t framer_st = (state & 0b0011111100000000) >> 8;
	bool pkt_flag = state & (1<<6);
	bool fifo_flag = state & (1<<5);

	logger.info("CRC=%d", crc_error);
	logger.info("FEC=%d", fec23_error);
	logger.info("FRAMER_ST=%d", framer_st);
	logger.info("PKT=%d", pkt_flag);
	logger.info("FIF=%d", fifo_flag);

	uint16_t fifo = readRegister(52);
	logger.info("FIFO_WR_PTR=%d", ((fifo>>8) & 0b111111));
	logger.info("FIFO_RD_PTR=%d", fifo & 0b111111);
}

uint64_t Lt8900v2::getSyncAddress(uint8_t address) const
{
	uint64_t sync = SYNCWORD;
	sync ^= ((uint64_t)address<<56 | (uint64_t)address<<48 | (uint64_t)address<<40 | (uint64_t)address<<32 | (uint64_t)address<<24 | (uint64_t)address<<16 | (uint64_t)address<<8 | (uint64_t)address);
	return sync;
}

void Lt8900v2::clearFifo() const
{
	writeRegister(52, 0x8080); //Fifo Rx/Tx queue reset
}
