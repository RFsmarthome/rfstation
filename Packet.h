/*
 * Packet.h
 *
 *  Created on: 06.03.2016
 *      Author: schnake
 */

#ifndef PACKET_H_
#define PACKET_H_

#include "schnakebus.h"

#include <string>
#include <memory>

class MqttClient;

class Packet
{
public:
	Packet();
	Packet(const buspacket_t &p);
	Packet(const char *buffer);
	virtual ~Packet();

	void setSource(uint8_t s);
	uint8_t getSource() const;

	void setDestination(uint8_t d);
	uint8_t getDestination() const;

	void setAck(bool a);
	bool getAck() const;

	void setReqAck(bool a);
	bool getReqAck() const;

	uint8_t getType() const;

	void setBuspacket(const buspacket_t &p);
	const buspacket_t& getBuspacket() const;

	buspacket_t getGeneratedBuspacket(uint8_t source = 0) const;
	buspacket4_t getGeneratedBuspacket4(uint8_t source = 0) const;

	bool isCrcCorrect() const;
	bool isCorrect() const;

	void dump() const;
	void dump(const buspacket_t &p) const;

	bool sendPacketToMqtt(std::shared_ptr<MqttClient> mqtt);

private:
	struct buspacket_t m_buspacket;
	uint8_t m_destination;
};


#endif /* PACKET_H_ */
