/*
 * Gpio.h
 *
 *  Created on: 23.04.2016
 *      Author: schnake
 */

#ifndef GPIO_H_
#define GPIO_H_

#include <string>

class Gpio {
public:
	Gpio();
	virtual ~Gpio();

	enum gpio_direction {
		IN,
		OUT
	};
	enum gpio_edge {
		FALLING,
		RISING,
		BOTH,
		NONE
	};

	int exportGpio(unsigned int gpio);
	int unexport(unsigned int gpio);
	int setDirection(unsigned int gpio, gpio_direction out_flag);
	int setValue(unsigned int gpio, bool value);
	bool getValue(unsigned int gpio);
	int setEdge(unsigned int gpio,  gpio_edge edge);
	bool poll(unsigned int gpio, int timeout);

private:
	std::string m_sysPath;
};

#endif /* GPIO_H_ */
