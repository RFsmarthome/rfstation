/*
 * Configuration.h
 *
 *  Created on: 26.09.2016
 *      Author: schnake
 */

#ifndef CONFIGURATION_H_
#define CONFIGURATION_H_

#include <string>

class Configuration
{
public:
	Configuration(const std::string &filename);
	virtual ~Configuration();

	const std::string& getMqttHost() const {
		return m_mqttHost;
	}
	int getMqttPort() const {
		return m_mqttPort;
	}
	int getMqttQos() const {
		return m_mqttQos;
	}
	unsigned int getLt89000Retransmission() const
	{
		return m_lt89000Retransmission;
	}
	unsigned int getLt8900Channel() const
	{
		return m_lt8900Channel;
	}
	unsigned int getLt8900PinPacket() const
	{
		return m_lt8900PinPacket;
	}
	unsigned int getLt8900PinReset() const
	{
		return m_lt8900PinReset;
	}
	const std::string& getLt8900SpiDev() const
	{
		return m_lt8900SpiDev;
	}
	unsigned int getLt8900SpiFrequency() const
	{
		return m_lt8900SpiFrequency;
	}
	const std::string& getMqttBase() const
	{
		return m_mqttBase;
	}
	bool isMqttRetain() const
	{
		return m_mqttRetain;
	}
	const std::string& getLog4cppFilename() const
	{
		return m_log4cppFilename;
	}

private:
	std::string m_filename;

	std::string m_log4cppFilename;

	std::string m_mqttHost;
	std::string m_mqttBase;
	int m_mqttPort;
	int m_mqttQos;
	bool m_mqttRetain;

	std::string m_lt8900SpiDev;
	unsigned int m_lt8900Channel;
	unsigned int m_lt8900SpiFrequency;
	unsigned int m_lt8900PinReset;
	unsigned int m_lt8900PinPacket;
	unsigned int m_lt89000Retransmission;

};

#endif /* CONFIGURATION_H_ */
