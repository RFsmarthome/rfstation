/*
 * Exception.h
 *
 *  Created on: 21.04.2016
 *      Author: schnake
 */

#ifndef EXCEPTION_H_
#define EXCEPTION_H_

#include <exception>
#include <string>

class Exception : public std::exception
{
public:
	Exception();
	Exception(const std::string &reason);
	virtual ~Exception();

	virtual const char* what() const throw();

protected:
	std::string m_reason;
};


#endif /* EXCEPTION_H_ */
